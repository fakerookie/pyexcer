class kls:
    @staticmethod
    def static():
        print("This is a static method in Class.")
        
    @classmethod
    def cmethod(self):
        print("This is a class method in Class.")

kls.static()
kls.cmethod()


class Point():
    def __init__(self, x=None, y=None):
        self.x = x
        self.y = y
    
    def __str__(self):
        return "(%d, %d)" % (self.x, self.y)
    
    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)
    
    def show(self):
        print("x: {}, y: {}".format(self.x, self.y))
        
class Vec2(Point):
    def __str__(self):
        return "Vec2(%.3f, %.3f)" % (self.x, self.y)
    
    # Operator Overwrite
    def __add__(self, other):
        return Vec2((self.x + other.x) * 0.1, (self.y + other.y) * 0.1)
    
    # Overwite
    def show(self):
        print("y: {} x: {}".format(self.y, self.x))

a = Point(2, 3)
b = Point(3, 2)
print(a + b)
a.show()


c = Vec2(2.3, 3.2)
d = Vec2(3.2, 2.3)
print(c + d)
d.show()
