#!/usr/bin/env python

http_response = b"""\
    <!DOCTYPE HTML>
    <html lang="en">
    <head>
    <title>Server Start Page</title>
    <style>
    body {
        background-color: black;
        text-align: center;
        color: skyblue;
        font-size: 30px;
    }
    </style>
    </head>
    <body>
      <h1>Hello WSGI Server With WSGI</h1>
    </body>
    </html>
    """

def app(environ, start_response):
    status = '200 ok'
    response_headers = [('Content-Type', 'text/html')]
    start_response(status, response_headers)
    return [http_response]
