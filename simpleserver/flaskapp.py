from flask import Flask

http_response = b"""\
    <!DOCTYPE HTML>
    <html lang="en">
    <head>
    <title>Server Start Page</title>
    <style>
    body {
        background-color: black;
        text-align: center;
        color: skyblue;
        font-size: 30px;
    }
    </style>
    </head>
    <body>
      <h1>Hello WSGI Server With Flask</h1>
    </body>
    </html>
    """

flask_app = Flask('flaskapp')

@flask_app.route('/')
def hello_world():
    return http_response

app = flask_app.wsgi_app
