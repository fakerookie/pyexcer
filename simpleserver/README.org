#+TITLE: ~Simple Server~

* Intro

Just a simple web server for exercise.

* Use:

- 1.py

#+BEGIN_SRC shell
./1.py
#+END_SRC

- 2.py

#+BEGIN_SRC
./2.py flaskapp:app

# or

./2.py wsgiapp:app
#+END_SRC
