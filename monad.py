#!/usr/bin/env python

class Monad:
    def __init__(self, value):
        self.value = value
    
    def bind(self, bind):
        return fn(self.value)
    
    @staticmethod
    def unit(value):
        return Monad(value)
    
    def __repr__(self):
        return f"Moand({self.value})"

def succ(v):
    return Monad(v + 1)

def double(v):
    return Monad(v * 2)

result = Monad.unit(2.5) \
         .bind(succ) \
         .bind(double) \
         .bind(succ)

print(result)
