import time
import functools

def tracetime(f):
    @functools.wraps(f)
    def clocked(*args, **kwargs):
        start   = time.perf_counter()
        result  = f(*args, **kwargs)
        elapsed = time.perf_counter() - start

        arglst = []
        if args:
            arglst.append(', '.join(repr(arg) for arg in args))

        if kwargs:
            pairs = ['%s = %r' % (k, v) for k, v in sorted(kwargs.items())]
            arglst.append(', '.join(repr(arg) for arg in kwargs))

        funname = f.__name__
        # argstr  = ', '.join(repr(arg) for arg in args)
        argstr  = ', '.join(arglst)

        print('[%0.8fs] %s(%s) -> %r' % (elapsed, funname, argstr, result))
        return result

    return clocked

@tracetime
def fact(n):
    return n if n < 2 else n * fact(n - 1)

@tracetime
def items(a=None, **kwargs):
    return (['%s : %s\n' % (k, v) for k, v in kwargs])

dic = {1: 'a', 2 : 'b', 3 : 'c', 4 : 'd', 5 : 'e'}

@functools.lru_cache() # () is must
# functools.lru_cache(maxsize=NUM, typed=True/False)
@tracetime
def fib(n):
    if n < 2:
        return n
    return fib(n - 2) + fib(n - 1)

print('5! = ', fact(5))
print('itmes function', items(dic))
print('Fib(5)', fib(5))

print('\nShow Values and FREE\'s Values of FUNCTION.')
print(tracetime.__name__ + ':')
print(tracetime.__code__.co_varnames)
print(tracetime.__code__.co_freevars)