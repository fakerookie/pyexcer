ss = "abcdefg"

[print(i) for i in enumerate(ss)]

print()
for j in enumerate(ss): print(j)

print()
[print(k) for k in enumerate(ss, 30)]

ll = ['a', 'b', 'c', 'd', 'e']
print("**")
[print(li) for li in enumerate(ll)]
print("*")
[print(lj) for _, lj in enumerate(ll)]
print("*")
[print(lk) for lk, _ in enumerate(ll)]
